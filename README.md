![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Gaea Nea web document
=====================

**Portable secure web document for the Gaea Nea group.**

A self-contained web document with embedded formatting, images, and public key, in a simple fluid/responsive format. Allows for easy verification and redistribution.

Unmodified redistribution allowed.

---

Todo: clean up CSS

